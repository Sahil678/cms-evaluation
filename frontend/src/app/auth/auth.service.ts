import { AuthData } from './auth-data.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  registered:boolean;
  private isAuthenticated = false;
  private token:string;
  private tokenTimer: NodeJS.Timer;
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) { }


getToken(){
  return this.token;
}

getAuthStatus(){
  return this.isAuthenticated;
}

getAuthStatusListener(){
  return this.authStatusListener.asObservable();
}


signUp(email: string , password: string){
  const authData: AuthData = {email: email, password: password};
  this.http.post("http://localhost:3000/api/user/signup",authData)
           .subscribe(res=>{
             console.log(res);
           });
}

login(email:string,password:string){
  const authData: AuthData = {email: email, password: password};
  this.http.post<{token:string,expiresIn:number}>("http://localhost:3000/api/user/login",authData)
      .subscribe(res=>{
        const token = res.token;
        this.token = token;
        if(token){
          const expiresInDuration = res.expiresIn;
          this.setAuthTimer(expiresInDuration);
          this.isAuthenticated = true;
        this.authStatusListener.next(true);
        const currentTime = new Date();
        const expirationTime = new Date(currentTime.getTime() +expiresInDuration*1000);
        this.saveAuthData(token,expirationTime);
       this.router.navigate(['/']);
      }
      });
}
  autoAuthUser(){
    const authInformation =  this.getAuthData();
    if(!authInformation){ return; }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
      if(expiresIn > 0){
        this.token = authInformation.token;
        this.isAuthenticated = true;
        this.setAuthTimer(expiresIn/1000);
        this.authStatusListener.next(true);
      }
  }

logout(){
  this.token = null;
  this.isAuthenticated = false;
  this.authStatusListener.next(false);
  clearTimeout(this.tokenTimer);
  this.clearAuthData();
  this.router.navigate(['/']);
}

private setAuthTimer(duration:number){
  console.log('Setting Timer: '+ duration);
  this.tokenTimer = setTimeout(()=>{
    this.logout();
  }, duration*1000);
}

private saveAuthData(token:string, expirationDate: Date){
       sessionStorage.setItem('token',token);
       sessionStorage.setItem('expiration', expirationDate.toISOString());

}

private clearAuthData(){
  sessionStorage.removeItem('token');
  sessionStorage.removeItem('expiration');
}
private getAuthData(){
  const token = sessionStorage.getItem('token');
  const expirationDate = sessionStorage.getItem('expiration');
  if(!token || !expirationDate){ return; }
  return{
    token:token,
    expirationDate: new Date(expirationDate)
  }
}
}
