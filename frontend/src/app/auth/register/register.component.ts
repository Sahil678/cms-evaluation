import { NgForm } from '@angular/forms';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  isLoading = false;
  registered:boolean;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  register(form:NgForm){
    if(form.invalid){return;}
    this.isLoading = true;
    this.authService.signUp(form.value.email,form.value.password);
    this.registered = true;

  }

}
