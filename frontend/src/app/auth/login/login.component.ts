import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
// import { UsersService } from '../../users.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoading = false;

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

login(form:NgForm){
if(form.invalid) {return};
this.isLoading = true;
this.authService.login(form.value.email,form.value.password);
// this.authService.signUp(form.value.email,form.value.password);
}

}
