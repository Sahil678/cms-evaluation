import { NewsCreateComponent } from './news/news-create/news-create.component';
import { NewsListComponent } from './news/news-list/news-list.component';
import { AuthGuard } from './auth/auth.guard';
import { RegisterComponent } from './auth/register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  {path:'',component: NewsListComponent},
  {path:'createnews',component: NewsCreateComponent, canActivate:[AuthGuard]},
  {path:'edit/:newsId',component: NewsCreateComponent, canActivate:[AuthGuard]},
  {path:'register', component: RegisterComponent, canActivate:[AuthGuard]},
  // {path:'newnews',component: NewnewsComponent, canActivate:[AuthGuard]},
  {path:'login',component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
