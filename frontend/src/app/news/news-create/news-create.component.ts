import { NewsService } from './../news.service';
import { Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NewsData } from '../news-data.model';

@Component({
  selector: 'app-news-create',
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.css']
})
export class NewsCreateComponent implements OnInit {
  private mode = 'create';
  private newsId: string;
  news: NewsData;

  constructor(public newsService:NewsService, public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap:ParamMap)=>{
      if(paramMap.has('newsId')){
        this.mode= 'edit';
        this.newsId = paramMap.get('newsId');
        this.news = this.newsService.getOneNews(this.newsId);
      }else{
        this.mode = 'create';
        this.newsId = null;
      }
    });
  }

  saveNews(form:NgForm){
    if(form.invalid){return;}
    if(this.mode==='create'){
      const news = {
        id: null,
        title: form.value.title,
        content: form.value.content
      };
      this.newsService.addNews(news);
    }else{
      this.newsService.updateNews(this.newsId,form.value.title,form.value.content);
    }

    form.reset();
  }

}
