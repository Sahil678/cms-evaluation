import { HttpClient } from '@angular/common/http';
import { NewsData } from './news-data.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private news: NewsData[] = [];
  private newsUpdated = new Subject<NewsData[]>();

  constructor(private http:HttpClient) { }

  getNews(){
    this.http.get<{message:String; news:any}>('http://localhost:3000/api/news')
    .pipe(map((newsData)=>{
      return newsData.news.map(news=>{
        return{
          title: news.title,
          content: news.content,
          id: news._id
        }
      })
    }))
    .subscribe(news =>{
      this.news =  news;
      this.newsUpdated.next([...this.news]);
    })
  }

  getNewsUpdateListener(){
    return this.newsUpdated.asObservable();
  }

  getOneNews(id:string){
    return {...this.news.find(n=>n.id===id)};
  }

  addNews(news: NewsData){
    this.http.post<{message:string}>('http://localhost:3000/api/news',news)
    .subscribe((responseData)=>{
      console.log(responseData);
      this.news.push(news);
      this.newsUpdated.next([...this.news]);
    });
  }

  updateNews(id:string, title:string, content:string ){
    const news:NewsData ={id:id, title:title, content: content};
    this.http.put('http://localhost:3000/api/news/'+id,news)
        .subscribe(response=>{
         console.log(response);
        })
  }

  deleteNews(newsId:string){
    this.http.delete('http://localhost:3000/api/news/'+newsId)
      .subscribe(()=>{
       const updatedNews = this.news.filter(newnews => newnews.id !== newsId);
       this.news = updatedNews;
       this.newsUpdated.next([...this.news]);
      })
  }
}
