import { Subscription } from 'rxjs';
import { NewsService } from './../news.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewsData } from '../news-data.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit,OnDestroy {
  public news: any=[];
  private newsSub: Subscription;
  private active= false;

  constructor(public newsService:NewsService) { }

  ngOnInit() {
    this.news = this.newsService.getNews();
    this.newsSub = this.newsService.getNewsUpdateListener()
        .subscribe((news: NewsData[])=>{
          this.news=news;
        });


  }

  ngOnDestroy(){
    this.newsSub.unsubscribe();
  }

  toggle(){
    this.active = !this.active;
  }

  deleteNews(newsId:string){
    this.newsService.deleteNews(newsId);
  }

}
