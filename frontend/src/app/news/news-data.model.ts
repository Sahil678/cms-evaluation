export interface NewsData{
  id: string,
  title: string,
  content: string
}
