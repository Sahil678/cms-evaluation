const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const crypto = require('crypto');
const User = require('./models/user');

const userRoutes = require('./routes/users');
const newsRoutes = require('./routes/news');

const app = express();

mongoose.connect('mongodb://localhost/CMS', { useNewUrlParser: true })
    .then(() => { console.log('Connected to MongoDB...') })
    .catch(err => console.log('Could not connect to MongoDB ', err));
// app.use((req, res, next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept");
//     res.setHeader("Access-Control-Allow-Methods", "*");
//     next();
// });
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

// app.post("/api/login", (req, res) => {
//     const salt = crypto.randomBytes(16).toString('hex')
//     const user = new User({
//         email: req.body.email,
//         salt: salt,
//         hash: crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, 'sha512').toString('hex')
//     });
//     console.log((req.body.password))
//     const salt = crypto.randomBytes(16).toString('hex');
//     console.log(crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, 'sha512').toString('hex'))
//     user.save();
//     res.status(201).json({ message: 'User Successfully logged' });
// });

app.use('/api/user', userRoutes);
app.use('/api/news', newsRoutes);

module.exports = app;