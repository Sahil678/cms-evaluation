const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const newsSchema = mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: true
    },
    content: {
        type: String,
        required: true
    }
});

newsSchema.plugin(uniqueValidator);
module.exports = mongoose.model('News', newsSchema);