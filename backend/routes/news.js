const express = require('express');
const router = express.Router();
const News = require('../models/news');


router.post('/', (req, res) => {
    const news = new News({

        title: req.body.title,
        content: req.body.content
    });
    news.save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'News Successfully Posted',
                result: result
            });
        }).catch(err => {
            res.status(500).json({
                message: 'An error Occurred',
                error: err
            });
        });
});

router.put('/:id', (req, res) => {
    const news = new News({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content
    })
    News.updateOne({ _id: req.params.id }, news).then(result => {
        console.log(result);
        res.send(200).json({ message: 'update successfull' })
    })
})

router.get('/', (req, res) => {
    News.find().then(documents => {
        res.status(200).json({
            message: "News Fetched Successfully",
            news: documents
        });
    });
});

router.delete('/:id', (req, res) => {
    News.deleteOne({ _id: req.params.id }).then(result => {
        console.log(result);
        res.status(200).json({
            message: 'News Successfully Deleted'
        });
    });
});

module.exports = router;