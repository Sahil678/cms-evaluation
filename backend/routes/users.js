const express = require('express');
const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

const router = express.Router();
const User = require('../models/user');
const checkAuth = require('../middleware/check-auth');

router.post('/signup', checkAuth, (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                password: hash
            });
            return user.save();
        }).then(result => {
            res.status(201).json({
                message: 'User Created',
                result: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                message: 'there was',
                error: err
            });
        });
});

router.post('/login', (req, res) => {
    let fetchedUser;
    User.findOne({ email: req.body.email })
        .then(user => {

            if (!user) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            fetchedUser = user;
            return bcrypt.compare(req.body.password, user.password);
        })
        .then(result => {
            if (!result) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            const token = jwt.sign({ email: fetchedUser.email, userId: fetchedUser._id }, 'this_is_a_secret_key_and_should_be_longer', { expiresIn: "1h" });
            res.status(200).json({
                token: token,
                expiresIn: 3600
            });
        })
        .catch(err => {
            return res.status(401).json({
                message: "Auth failed",
                err: err
            });
        })
});


module.exports = router;